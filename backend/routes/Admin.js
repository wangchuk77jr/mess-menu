const express = require("express");
const router = express.Router();
const multer = require("multer");
const path = require("path");

const { pool } = require("../db");

// File upload configuration
const storage = multer.diskStorage({
    destination: "./uploads/",
    filename: (req, file, cb) => {
      const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
      const fileExtension = path.extname(file.originalname);
      cb(null, file.fieldname + "-" + uniqueSuffix + fileExtension);
    },
  });
  
  const upload = multer({ 
    storage,
    fileFilter: (req, file, cb) => {
      // Check if uploaded file is an image
      if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return cb(new Error('Only image files are allowed!'), false);
      }
      cb(null, true);
    }
  });
  
  // post meal image and name routes
  
  // Create blog post route (POST)
  router.post("/api/postMeals", upload.single("image"), async (req, res) => {
    try {
      const { mealCategory, mealName } = req.body;
  
      // Check if required fields are missing
      if (!mealCategory || !mealName || !req.file) {
        return res.status(400).json({ error: 'Meal category, name, and image are required!' });
      }
  
      const mealImage = req.file.filename;
  
      const query = `INSERT INTO meals (mealCategory, mealImage, mealName) 
                     VALUES ($1, $2, $3) RETURNING *`;
      const values = [mealCategory, mealImage, mealName];
  
      const result = await pool.query(query, values);
      const createdMeals = result.rows[0];
  
      // Send success message along with created meal
      res.status(201).json({ message: 'Meal created successfully', meal: createdMeals });
    } catch (error) {
      console.error("Error creating meals:", error);
      res.status(500).json({ error: "Internal server error" });
    }
  });

// Retrieve all meals route (GET)
router.get("/api/getMeals", async (req, res) => {
    try {
      // Construct SQL query to select all meals from the database
      const query = "SELECT * FROM meals";
  
      // Execute SQL query
      const result = await pool.query(query);
  
      // Retrieve meals from query result
      const meals = result.rows;
  
      // Send retrieved meals in the response
      res.status(200).json({ meals });
    } catch (error) {
      console.error("Error fetching meals:", error);
      res.status(500).json({ error: "Internal server error" }); // Send 500 error for any unexpected errors
    }
  });
  
// Route to get all feedbacks
router.get('/api/getFeedbacks', async (req, res) => {
    try {
        const feedbacks = await pool.query('SELECT * FROM feedbacks ORDER BY created_at DESC');
        res.json(feedbacks.rows);
    } catch (error) {
        console.error('Error fetching feedbacks:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
});

// Route to post notifications
router.post("/api/postNotification", async (req, res) => {
    try {
        const { subject, message } = req.body;

        // Check if required fields are present
        if (!subject || !message) {
            return res.status(400).json({ error: "subject, and message are required" });
        }

        const query = `INSERT INTO notifications (subject, message) VALUES ($1, $2) RETURNING *`;
        const values = [subject, message];

        const result = await pool.query(query, values);
        const createdNotification = result.rows[0];

        // Send success message with the created notification
        res.status(201).json({ message: "Notification has been posted successfully", notification: createdNotification });
    } catch (error) {
        console.error("Error while posting notification:", error);
        res.status(500).json({ error: "Internal server error" });
    }
});

// Route to get all todays'menu
router.get('/api/getTodaysMenu', async (req, res) => {
    try {
        const feedbacks = await pool.query('SELECT * FROM mess_menus WHERE menuCategory = \'todaysMenu\' ORDER BY CASE dayName WHEN \'Monday\' THEN 1 WHEN \'Tuesday\' THEN 2 WHEN \'Wednesday\' THEN 3 WHEN \'Thursday\' THEN 4 WHEN \'Friday\' THEN 5 WHEN \'Saturday\' THEN 6 WHEN \'Sunday\' THEN 7 END');
        res.json(feedbacks.rows);
    } catch (error) {
        console.error('Error fetching Today\'s menu:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
});

// Route to get all late classes menu
router.get('/api/getLateClassesMenu', async (req, res) => {
    try {
        const feedbacks = await pool.query('SELECT * FROM mess_menus WHERE menuCategory = \'lateClassesMenu\' ORDER BY CASE dayName WHEN \'Monday\' THEN 1 WHEN \'Tuesday\' THEN 2 WHEN \'Wednesday\' THEN 3 WHEN \'Thursday\' THEN 4 WHEN \'Friday\' THEN 5 WHEN \'Saturday\' THEN 6 WHEN \'Sunday\' THEN 7 END');
        res.json(feedbacks.rows);
    } catch (error) {
        console.error('Error fetching Late classses menu:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
});


// route to upadte menu
router.put('/api/updateMenu/', async (req, res) => {
    const { id, breakfast, lunch, dinner } = req.body;
    if (!id) {
        return res.status(400).json({ success: false, message: 'ID is required' });
    }

    try {
        const client = await pool.connect();
        const result = await client.query('UPDATE mess_menus SET breakfast = $1, lunch = $2, dinner = $3 WHERE id = $4 RETURNING *', [breakfast, lunch, dinner, id]);
        client.release();

        if (result.rowCount === 1) {
            res.json({ success: true, message: 'Menu item updated successfully', updatedMenu: result.rows[0] });
        } else if (result.rowCount === 0) {
            res.status(404).json({ success: false, message: 'Menu item not found' });
        } else {
            // This case should never happen if your database schema is correct
            res.status(500).json({ success: false, error: 'Unexpected result: Multiple rows updated' });
        }
    } catch (error) {
        console.error('Error updating menu item:', error);
        res.status(500).json({ success: false, error: 'Internal server error' });
    }
});




module.exports = router;
