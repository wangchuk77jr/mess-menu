const express = require("express");
const router = express.Router();
// const multer = require("multer");
// const path = require("path");

const { pool } = require("../db");

// post feedabck
router.post("/api/postFeedback", async (req, res) => {
    try {
        const { name, subject, message } = req.body;

        // Check if required fields are present
        if (!name || !subject || !message) {
            return res.status(400).json({ error: "Name, subject, and message are required" });
        }

        const query = `INSERT INTO feedbacks (name, subject, message) VALUES ($1, $2, $3) RETURNING *`;
        const values = [name, subject, message];

        const result = await pool.query(query, values);
        const createdFeedback = result.rows[0];

        // Send success message with the created feedback
        res.status(201).json({ message: "Feedback submitted successfully", feedback: createdFeedback });
    } catch (error) {
        console.error("Error while submitting feedback:", error);
        res.status(500).json({ error: "Internal server error" });
    }
});

// Route to get all feedbacks
router.get('/api/notifications', async (req, res) => {
    try {
        const feedbacks = await pool.query('SELECT * FROM notifications ORDER BY created_at DESC');
        res.json(feedbacks.rows);
    } catch (error) {
        console.error('Error fetching notifications:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
});

module.exports = router;
