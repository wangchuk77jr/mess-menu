-- psql -h localhost -U postgres
-- \l
-- \c database name
-- \dt 


-- To Create database 
CREATE DATABASE "gcitmess";

-- Create User Table
CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    email VARCHAR(150) UNIQUE NOT NULL,
    userType VARCHAR(20) DEFAULT 'student' NOT NULL,
    password VARCHAR(100) NOT NULL
);
-- Create Menu Table
CREATE TYPE menu_category AS ENUM ('todaysMenu', 'lateClassesMenu');
CREATE TABLE mess_menus (
    id SERIAL PRIMARY KEY,
    dayName VARCHAR(150) NOT NULL,
    menuCategory menu_category NOT NULL,
    breakFast VARCHAR(100) NOT NULL,
    lunch VARCHAR(100) NOT NULL,
    dinner VARCHAR(100) NOT NULL
);

INSERT INTO mess_menus (dayName, menuCategory, breakFast, lunch, dinner) 
VALUES 
('Monday', 'todaysMenu', 'Plain rice with cabbage azay', 'Fried rice with boiled egg', 'Plain rice with beans'),
('Tuesday', 'todaysMenu', 'Fried rice', 'Plain rice with potato', 'Plain rice with mixed vegetables'),
('Wednesday', 'todaysMenu', 'Fried rice with cabbage ezay', 'Plain rice with chicken', 'Plain rice with potato'),
('Thursday', 'todaysMenu', 'Fried rice with ezay', 'Plain rice with ema datshi', 'Plain rice with spinach'),
('Friday', 'todaysMenu', 'Fried rice', 'Plain rice with beef', 'Plain rice with cauliflower'),
('Saturday', 'todaysMenu', 'Plain rice with ezay', 'Plain rice with mixed-vegetables', 'Plain rice with mushroom'),
('Sunday', 'todaysMenu', 'Fried rice', 'Plain rice with fried cabbage', 'Plain rice with brinjil');


INSERT INTO mess_menus (dayName, menuCategory, breakFast, lunch, dinner) 
VALUES 
('Monday', 'lateClassesMenu', 'Plain rice with cabbage azay', 'Fried rice with boiled egg', 'Plain rice with beans'),
('Tuesday', 'lateClassesMenu', 'Fried rice', 'Plain rice with potato', 'Plain rice with mixed vegetables'),
('Wednesday', 'lateClassesMenu','Fried rice', 'Plain rice with chicken', 'Plain rice with potato'),
('Thursday', 'lateClassesMenu', 'Fried rice with azay', 'Plain rice with ema datshi', 'Plain rice with spinach'),
('Friday', 'lateClassesMenu', 'Fried rice', 'Plain rice with beef', 'Plain rice with cauliflower'),
('Saturday', 'lateClassesMenu', 'Plain rice with azay', 'Plain rice with mixed-vegetables', 'Plain rice with mushroom'),
('Sunday', 'lateClassesMenu', 'Fried rice', 'Plain rice with fried cabbage', 'Plain rice with brinjil');

-- Create feedback Table
CREATE TABLE feedbacks (
    id SERIAL PRIMARY KEY,
    name VARCHAR(150) NOT NULL,
    subject VARCHAR(200) NOT NULL,
    message TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Create Notification Table
CREATE TABLE notifications (
    id SERIAL PRIMARY KEY,
    subject VARCHAR(200) NOT NULL,
    message TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


-- Create Meal Table
CREATE TYPE meal_category AS ENUM ('veg', 'non_veg','winter','summer');
CREATE TABLE meals (
    id SERIAL PRIMARY KEY,
    mealCategory meal_category NOT NULL,
    mealImage VARCHAR(1000) NOT NULL,
    mealName VARCHAR(200) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


