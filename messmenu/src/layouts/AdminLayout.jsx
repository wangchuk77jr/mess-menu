import React from 'react'
import LogoImage from '../assets/logo.png';
import './Layout.css';

import { NavLink,useNavigate} from 'react-router-dom'

const AdminLayout = ({children}) => {
    const navigate = useNavigate();
    const handleLogout = () => {
      // Clear user authentication data from local storage
      localStorage.clear();
      // Navigate to the login page
      navigate('/login');
      window.location.reload();
    };
  
  return (
    <div className='min-vh-100 mb-0'>
        {/* Header Section */}
        <header style={{zIndex:'999'}} className="navbar navbar-expand-lg bg-primary position-sticky top-0">
            <div className="container-fluid px-md-5 px-3">
                <NavLink className="navbar-brand" to="/home">
                    <img className='img-fluid' src={LogoImage} alt="logo" />
                </NavLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarScroll">
                    <ul className="navbar-nav ms-auto my-2 my-lg-0 navbar-nav-scroll">
                        <li className="nav-item">
                            <NavLink className="nav-link text-uppercase text-white" aria-current="page" to="/gcitadminmenuhome">Dashboard</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link text-uppercase text-white" aria-current="page" to="/gcitadminmenumessmenu">mess menu</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link text-uppercase text-white" aria-current="page" to="/gcitadminmenufeedback">feedback</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link text-uppercase text-white" aria-current="page" to="/gcitadminmenunotification">Notification</NavLink>
                        </li>
                        <li className="nav-item">
                            <button onClick={handleLogout} className="nav-link text-uppercase text-white">log out</button>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        {/* Children section */}
        <section className='min-vh-100'>
            {children}
        </section>

        {/* Footer section */}
        <footer className='bg-primary' style={{minHeight:'35vh'}}>
            <div className="container-fluid px-md-5 px-3 pt-4">
                <div className="d-flex justify-content-between flex-wrap">
                    <p style={{fontSize:'20px',}} className='text-white fw-semibold'>Get connect with us on social networks: </p>
                    <div className='ms-md-auto'>
                        <ul className='list-unstyled d-flex  align-items-center gap-3 me-5'>
                            <li className="nav-item">
                                <NavLink to="#">
                                    <box-icon size="md" name='facebook-square' type='logo' color='#fffdfd' ></box-icon>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="#">
                                    <box-icon size="md" name='instagram' type='logo' color='#fffdfd' ></box-icon>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="#">
                                  <box-icon size="md" name='twitter' type='logo' color='#fffdfd' ></box-icon>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="#">
                                    <box-icon size="md" name='youtube' type='logo' color='#fffdfd' ></box-icon>
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="row mt-2 g-2">
                    <div className="col-md-6">
                        <div>
                            <img width={200} className='ms-md-5' src={LogoImage} alt="logo" /> 
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div>
                            <p className='text-white'>Links</p>
                            <NavLink className="fs-6 fw-bold text-white text-decoration-none">GCIT</NavLink>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div className='d-flex flex-column'>
                            <p className='text-white'>Contact</p>
                            <NavLink className="fs-6 fw-bold text-white text-decoration-none d-flex align-items-center gap-2">
                                <box-icon name='phone-call' type='solid' color='#fffdfd' ></box-icon>
                                <span>17656789</span>
                            </NavLink>
                            <NavLink className="fs-6 mt-2 fw-bold text-white text-decoration-none  d-flex align-items-center gap-2">
                                <box-icon name='envelope' type='solid' color='#fffdfd' ></box-icon>
                                <span>GCITMess@gmail.com</span>
                            </NavLink>
                        </div>
                    </div>
                    <div className='col-md-2'>
                        <div className='d-flex flex-column'>
                            <p className='text-white'>Location</p>
                            <NavLink className="fs-6 fw-bold text-white text-decoration-none  d-flex align-items-center gap-2">
                                <box-icon name='map' type='solid' color='#fffdfd' ></box-icon>
                                <span>Kabesa,Thimphu</span>
                            </NavLink>
                        </div>
                    </div> 
                </div>
                {/* copyright */}
                <p className='w-100 text-center text-white'>GCIT-MESS-Management 2024. All Rights Reserved.</p>
            </div>
        </footer>
    </div>
  )
}

export default AdminLayout