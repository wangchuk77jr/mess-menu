import React from 'react';
import { BrowserRouter as Router, Routes, Route, Navigate} from "react-router-dom";
import LoginForm from "./components/auth/LoginForm";
// import Home from "./components/menu/Home";
// import MessMenu from "./components/menu/MessMenu";
// import FeedBack from "./components/menu/FeedBack";
import About from "./components/menu/About";
// import Notification from "./components/menu/Notification";
import SignupForm from "./components/auth/SignupForm";
// import AdminHome from './components/Admin/Adminmenu/Adminhome';
import AdminNotification from './components/Admin/Adminmenu/Adminnotification';
import AdminMessMenu from "./components/Admin/Adminmenu/Adminmessmenu";
// import AdminFeedback from "./components/Admin/Adminmenu/Adminfeedback";
import PrivateRoutes from './utils/PrivateRoutes';

// New Student Pages
import Home from './pages/students/Home';
import Notification from './pages/students/Notification';
import MessMenu from './pages/students/MessMenu';
import FeedBack from './pages/students/FeedBack';

// New Admin page
import AdminHome from './pages/admin/Home';
import AdminFeedback from './pages/admin/Adminfeedback';


function App() {
  // Retrieve authentication status and user type from local storage
  const isAuthenticated = localStorage.getItem("jwtToken") !== null;
  const userType = localStorage.getItem('userType'); // student or admin

  return (
    <>
    <Router>
      <Routes>
        {/* Route to PrivateRoutes for authenticated users */}
        <Route element={<PrivateRoutes isAuthenticated={isAuthenticated} />}>
          {/* Private Routes for student users */}
          {userType === 'student' && (
            <>
              <Route path="/home" element={<Home />} />
              <Route path="/messmenu" element={<MessMenu />} />
              <Route path="/feedback" element={<FeedBack />} />
              <Route path="/about" element={<About />} />
              <Route path="/notification" element={<Notification />} />
            </>
          )}
          {/* Private Routes for admin */}
          {userType === 'admin' && (
            <>
              <Route path="/gcitadminmenuhome" element={<AdminHome />} />
              <Route path="/gcitadminmenunotification" element={<AdminNotification />} />
              <Route path="/gcitadminmenumessmenu" element={<AdminMessMenu />} />
              <Route path="/gcitadminmenufeedback" element={<AdminFeedback />} />
            </>
          )}
        </Route>
        
        {/* Redirect authenticated users from login and signup pages */}
        {isAuthenticated && (
          <>
            <Route path="/login" element={userType === 'admin' ? <Navigate to="/gcitadminmenuhome" /> : <Navigate to="/home" />} />
            <Route path="/signup" element={userType === 'admin' ? <Navigate to="/gcitadminmenuhome" /> : <Navigate to="/home" />} />
          </>
        )}

        {/* Route to login and signup pages for unauthenticated users */}
        {!isAuthenticated && (
          <>
            <Route exact path="/login" element={<LoginForm />} />
            <Route path="/signup" element={<SignupForm />} />
          </>
        )}
        
        {/* Default route when no match is found */}
        <Route path="*" element={<Navigate to="/login" />} />
      </Routes>
    </Router>
    </>
  );
}

export default App;
