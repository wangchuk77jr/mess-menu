import React, { useState, useEffect } from 'react';
import './Login.css';
import Food from '../Assets/pic1.png';
import Food2 from '../Assets/pic2.jpeg';
import Food3 from '../Assets/pic3.jpg';
import logo from '../Assets/logo.png';
import { Link } from 'react-router-dom';
import { useNavigate, useLocation } from 'react-router-dom';
import axios from 'axios';
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const LoginForm = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const successMessage = location.state && location.state.successMessage;

  useEffect(() => {
    if (successMessage) {
      // Clear any existing toasts
      toast.dismiss();

      // Show a success toast with the provided message
      toast.success(successMessage);
    }
  }, [successMessage]);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmitLogin = async (e) => {
    e.preventDefault();

    try {
      // Replace with your actual server endpoint for login
      const response = await axios.post("http://localhost:5000/login", {
        email,
        password,
      });
      if (response.data.success){
        
      }
      const { token, userType } = response.data;

      // Store the token in localStorage
      localStorage.setItem("jwtToken", token);
      localStorage.setItem("userType", userType);
      
      // Show a success toast
      toast.success("Login successful!");

      window.location.reload();
      // Perform a full-page refresh
      navigate("/home", {
        state: {
          successMessage: "Logged in successfully.",
        },
      });
    } catch (error) {
      console.error("Login failed", error.response?.data);

      // Display an error toast to the user
      toast.error(
        error.response?.data?.message ||
          "Login failed. Please check your email and password."
      );
    }
  };

  return (
    <>
    <div className='wrapper'>
      <img src={Food} alt="Food" className="food-image" />
      <img src={Food2} alt="Food" className="food-image2" />
      <img src={Food3} alt="Food" className="food-image3" />
      <div className='container-fluid' style={{zIndex:'9999'}}>
        <div className='row d-flex justify-content-center align-items-center'>
          <form className='col-md-3' onSubmit={handleSubmitLogin}>
            <div className='d-flex justify-content-center align-items-center mb-3'>
              <img className='text-center' src={logo}  />
            </div>
              <h4 className='text-primary text-center'>LOGIN TO YOUR ACCOUNT</h4>
              <hr style={{border:'1px solid #DD7D25'}} />
              <input className='form-control border border-primary text-primary mb-3' required type="email" placeholder="example.gcit@rub.edu.bt" value={email} onChange={(e) => setEmail(e.target.value)} />
              <input className='form-control border border-primary text-primary mb-3' required type="password" placeholder='Password' value={password} onChange={(e) => setPassword(e.target.value)} />
              <button className='form-control btn btn-primary text-white' type="submit">LOGIN</button>
            <div className="register-link mt-3 text-center">
              <p>Don't have an account? <Link to="/signup">Sign Up</Link></p>
            </div>
          </form>
        </div>
      </div>
    </div>
    <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored"
    />
    </>
  );
};

export default LoginForm;
