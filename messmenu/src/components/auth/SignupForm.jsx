import React, { useState } from 'react';
import './Login.css';
import Food from '../Assets/pic1.png';
import Food2 from '../Assets/pic2.jpeg';
import Food3 from '../Assets/pic3.jpg';
import logo from '../Assets/logo.png';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const SignupForm = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [emailError, setEmailError] = useState('');
    const [passwordError, setPasswordError] = useState('');
    const [confirmPasswordError, setConfirmPasswordError] = useState('');
    const navigate = useNavigate();

    const handleEmailChange = (e) => {
        const value = e.target.value;
        setEmail(value);
        setEmailError('');

        // Validate email format
        if (!/^\d{8}\.gcit@rub\.edu\.bt$/.test(value)) {
            setEmailError('Email format is invalid. It should be in the format "xxxxxxxx.gcit@rub.edu.bt"');
        }
    };

    const handlePasswordChange = (e) => {
        const value = e.target.value;
        setPassword(value);
        setPasswordError('');

        // Validate password format
        if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()])[A-Za-z\d!@#$%^&*()]{8,15}$/.test(value)) {
            setPasswordError('Password must be at least 8 characters long, maximum 15 characters, containing at least one uppercase letter, one lowercase letter, one number, and one special character.');
        }
    };

    const handleConfirmPasswordChange = (e) => {
        setConfirmPassword(e.target.value);
        setConfirmPasswordError('');
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        
        // Validation
        if (password !== confirmPassword) {
            setConfirmPasswordError('Passwords do not match');
            return;
        }
    
        if (!emailError && !passwordError && !confirmPasswordError) {
            try {
                const response = await axios.post('http://localhost:5000/register', {
                    email,
                    password,
                });
                // Check if 'data' property exists in the response
                if (response.data.success) {
                    // Destructure token and userType from response.data
                    const { token, user } = response.data;

                    // Store the token in localStorage
                    localStorage.setItem("jwtToken", token);
                    // Store the userType in localStorage
                    localStorage.setItem("userType", user.usertype);

                    window.location.reload();
                    // Redirect to login page
                    navigate("/home", {
                    state: {
                        successMessage: "User registered successfully.",
                    }
                    
                    });
                } else {
                    // Show an error toast if 'data' property is missing in the response
                    toast.error(
                    response.data.message || "Registration failed. Please try again."
                    );
                    console.error("Invalid response format", response);
                }
            } catch (error) {
                // Check if 'response' property exists in the error object
                if (error.response) {
                    // Show error toast with the error message from the server
                    toast.error(`Registration failed: ${error.response.data.message}`);
                    console.error("Registration failed", error.response.data);
                } else {
                    // Show a generic error toast for other types of errors
                    toast.error("Registration failed. Please try again.");
                    console.error("Unexpected error", error);
                }
            }
        }
    };
    

    return (
        <>
        <div className='wrapper'>
            <img src={Food} alt="Food" className="food-image" />
            <img src={Food2} alt="Food" className="food-image2" />
            <img src={Food3} alt="Food" className="food-image3" />
            <img src={logo} alt="Food" className="logo" />
            <div className='container-fluid' style={{zIndex:'9999'}}>
                <div className='row d-flex justify-content-center align-items-center'>
                    <form className='col-md-3' onSubmit={handleSubmit}>
                        <div className='d-flex justify-content-center align-items-center mb-3'>
                            <img className='text-center' src={logo}  />
                        </div>
                        <h4 className='text-primary text-center'>Create a new account</h4>
                        <hr style={{border:'1px solid #DD7D25'}} />
                        <input  className='form-control border border-primary text-primary mb-3'  type="email" placeholder="xxxxxxxx.gcit@rub.edu.bt" required value={email} onChange={handleEmailChange}/>
                        {emailError && <p className="text-danger">{emailError}</p>}
                        
                        <input className='form-control border border-primary text-primary mb-3' type="password" placeholder='Password' required value={password} onChange={handlePasswordChange}/>
                        {passwordError && <p className="text-danger">{passwordError}</p>}

                        <input className='form-control border border-primary text-primary mb-3' type="password" placeholder='Confirm Password' required value={confirmPassword} onChange={handleConfirmPasswordChange}/>
                        {confirmPasswordError && <p className="text-danger">{confirmPasswordError}</p>}

                        <button className='form-control btn btn-primary text-white' type="submit">SIGN UP</button>
                        <div className="register-link text-center mt-3">
                            <p>Already have an account? <Link to="/login">Log In</Link></p>
                        </div>
                    
                    </form>
                </div>
            </div>

        </div>
        <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
            />
        </>
    );
};

export default SignupForm;
