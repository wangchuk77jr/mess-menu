// import React, { useState, useEffect } from "react";
import React from 'react';
import BackGroundImage from "../Assets/background.png";
import AboutImage from '../Assets/about-image.png';
import TeamMember1 from '../Assets/member1.png';
import TeamMember2 from '../Assets/member2.png';
import TeamMember3 from '../Assets/member3.png';
import TeamMember4 from '../Assets/member4.png';
import circle from '../Assets/circle.png';
import circle2 from '../Assets/circle2.png';
import circle3 from '../Assets/circle3.png';
import circle4 from '../Assets/circle4.png';
// import logo from '../Assets/logowhite.png';
import Footer from "../bar/Footer";
import "./About.css";
import TopNav from "../bar/TopNav";
import { FaLongArrowAltDown } from "react-icons/fa";

const About = () => {
    return(
      <div className="about-section">
            <TopNav/>
        <div className='about-section-text'>
            <p className='subheading'>About <span className='us-text1'>us</span></p>
        <br></br><br></br>
        <h1 className="heading">
            Welcome to the Mess Menu Displayer, your go-to platform for staying
            <br></br>
            informed about the latest meal plans and updates at Gyalpozhing
            <br></br>
            College of Information Technology(GCIT)
        </h1>
        <br></br>
        <div className="about-buttons-container">
            <button className="secondary-button">
                Know More <FaLongArrowAltDown />
            </button>
        </div>
        </div>

        <div className="chef">
          <img src={AboutImage} alt="" />
        
        <div className="about-background-image">
          <img src={BackGroundImage} alt="" />
        </div>
        </div>
        <div className="content">
        <p>The Mess Menu Displayer offers a comprehensive solution to the challenges faced by students regarding meal planning and transparency in the college  mess </p>
        <p> community. Through real-time updates and a user-friendly interface, students can easily access information about daily meal plans, ensuring  transparency and </p>
        <p>eliminating surprises. With secure login access, users can provide feedback on meals, which is integrated to continually  improve menu offerings based on user</p>
        <p> preferences. Additionally, the platform notifies users about any delays or changes in the menu, allowing for better scheduling and coordination. Overall, the Mess  </p>
        <p> Menu Displayer aims to enhance the dining experience for students by providing timely information and promoting a more interactive and cooperative dining</p>
        <p> environment.</p>
        </div>
        <div className="team">
        <h1 className="team1">Meet<span className="team2">our team</span></h1>
        <div className="team-members">
          <div>
            <img src={TeamMember1} alt="Team Member 1" />
            <p className="description1">Jolly Jigme 
            <br></br><br></br>
            <span className="second-text1">Lead developer</span>
            </p>
            </div>
            <div>
            <img src={TeamMember2} alt="Team Member 2" />
            <p className="description2">Jocular Jigme
            <br></br><br></br>
            <span className="second-text2">Project Manager</span>
            </p>
            </div>
            <div>
            <img src={TeamMember3} alt="Team Member 3" />
            <p className="description3">Kinetic Kinley
            <br></br><br></br>
            <span className="second-text3">UX/UI Designer</span>
            </p>
            </div>
            <div>
            <img src={TeamMember4} alt="Team Member 4" />
            <p className="description4">Knockout Kinley
            <br></br><br></br>
            <span className="second-text4">Quality Assurance Specialist</span>
            </p>
           
            </div>
          </div>
        </div>
        <hr className="styled-hr"></hr>
        <div className='model'></div>
      <img src={circle} alt="" className="team-circle" />
      <hr className="hr"></hr>
      <div className="aim-text">
      <ul>
         <li>To build a responsive web application for GCIT students to check upcoming meals and stay updated on mess related issues.</li>
      </ul>
      </div>
      <hr className="hr1"></hr>
      <img src={circle2} alt="" className="team-circle2" />
      <hr className="hr"></hr>
      <div className="aim-text2">
      <ul>
         <li>To give the menu plan beforehand.</li>
         <li>To inform students about delays in meals and other issues.</li>
         <li>Upgrade the menu based on student’s feedback.</li>
      </ul>
      </div>

      <hr className="hr3"></hr>
      <div className="additional-line">
        <hr className="li"></hr>
      </div>
       
    
      <img src={circle3} alt="" className="team-circle3" />

      <div className="aim-text3">
      <ul>
         <li>Establish online presence for mess accessibility.</li>
         <li>Provide meal schedules, menus, and event details.</li>
         <li>Improve website user experience.</li>
      </ul>
      </div>
      <div className="additional-line2">
        <hr className="li2"></hr>
      </div>

      <div className="additional-line3">
        <hr className="li3"></hr>
      </div>
        
      
        
  <img src={circle4} alt="" class="team-circle4" />

  <div className="additional-line0">
        <hr className="li0"></hr>
      </div>
       
  <div class="aim-text4">
    <ul>
       <li>Allows users to login securely.</li>
       <li>Display weekly menus.</li>
       <li>Collect user feedback on meals.</li>
       <li>Collect user feedback on meals.</li>
       <li>Use feedback to improve menu offerings.</li>
    </ul>

</div>
<div className="aboutfooter">
        <Footer />
        </div>
      </div>
      

    )
}

export default About;
