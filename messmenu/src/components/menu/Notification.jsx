import React, {useState,useEffect} from "react";
import Footer from "../bar/Footer";
import axios from "axios";
import TopNav from "../bar/TopNav";


import "./Notification.css";

const Notification = () => {
  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    // Fetch notifications when the component mounts
    const fetchNotifications = async () => {
      try {
        const response = await axios.get("http://localhost:5000/api/notifications");
        setNotifications(response.data);
      } catch (error) {
        console.error("Error fetching notifications:", error);
      }
    };
    fetchNotifications();
  }, []); // Empty dependency array to run effect only once on mount

    return (
      <div className="about-section-container">
        <TopNav/>
      
        <div className="notifications-container">
        {notifications.map((n, index) => (
          <div key={index} className="w-100 bg-body-secondary p-3 rounded gap-2 mb-3">
            (Notification)
            <h5 className="mt-3">Subject: {n.subject}</h5>
            <h5>Message: {n.message}</h5>
          </div>
        ))}
      </div>
      
      <footer className="notififooter">
    <Footer />
    </footer>
      </div>
    );
  };

export default Notification;
