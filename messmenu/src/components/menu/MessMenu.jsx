import React ,{useEffect,useState} from 'react';
import "./MessMenu.css";
import mess from "../Assets/mess.png";
import Footer from "../bar/Footer";
import background from "../Assets/background.png";
import TopNav from "../bar/TopNav";
import axios from 'axios';


const MessMenu = () => {
    const [todaysMenu, setTodaysMenu] = useState([]);
    const [LateClassesMenu, setLateClassesMenu] = useState([]);

    useEffect(() => {
        const fetchTodaysMenu = async () => {
            try {
                const response = await axios.get('http://localhost:5000/api/getTodaysMenu');
                setTodaysMenu(response.data);
            } catch (error) {
                console.error('Error fetching today\'s menu:', error);
            }
        };
        fetchTodaysMenu();
    }, []);

    useEffect(() => {
        const fetchLateClassesMenu = async () => {
            try {
                const response = await axios.get('http://localhost:5000/api/getLateClassesMenu');
                setLateClassesMenu(response.data);
            } catch (error) {
                console.error('Error fetching late classes menu:', error);
            }
        };
        fetchLateClassesMenu();
    }, []);
   
    return (
        <>
         <TopNav/>
         <div className="about-background-image">
           
    <img src={background} alt="" />
  </div>
        <div className="mess-text-container">
         <h1 className="mess-heading">Our <span className='mess-heading2'>menu</span></h1>
         <img src={mess} alt="mess" className="mess-herobanner"/>
           <div className='mess-text'>
               <p className="mess-para1">Welcome to our menu page! Our menu offers a delightful array of culinary creations </p>
               <p className="mess-para2">designed to tantalize your taste buds and satisfy your cravings. From hearty breakfast</p>
               <p className="mess-para3">  options to mouthwatering lunch and dinner selections, we have something </p>
               <p className="mess-para4"> to suit every palate.</p>
            </div>
        </div>
        <div className="whole-table">
            
            <h1 className='m1'>Todays menu</h1>
            <table className="menu-table">
                <thead>
                    <tr className="Head">
                        <th>Day</th>
                        <th>Breakfast (7:00 a.m. - 9:00 a.m.)</th>
                        <th>Lunch (12:00 p.m. - 2:00 p.m.)</th>
                        <th>Dinner (7:15 p.m. - 8:00 p.m.)</th>
                    </tr>
                </thead>
                <tbody>
                {todaysMenu.map(menu => (
                            <tr key={menu.id}>
                                <td>{menu.dayname}</td>
                                <td>{menu.breakfast || ""}</td>
                                <td>{menu.lunch || ""}</td>
                                <td>{menu.dinner || ""}</td>
                            </tr>
                        ))}
                </tbody>
            </table>
            <div className='second-table'>
            <h1 className="mt-5">For late classes</h1>
            <table className="menu-table">
                <thead>
                    <tr className="Head"> 
                        <th>Day</th>
                        <th>Breakfast (8:30 a.m. - 9:00 a.m.)</th>
                        <th>Lunch (2:00 p.m. - 3:00 p.m.)</th>
                        <th>Dinner (8:00 p.m. - 8:30 p.m.)</th>
                    </tr>
                </thead>
                        <tbody>
                            {LateClassesMenu.map(menu => (
                                <tr key={menu.id}>
                                    <td>{menu.dayname}</td>
                                    <td>{menu.breakfast || ""}</td>
                                    <td>{menu.lunch || ""}</td>
                                    <td>{menu.dinner || ""}</td>
                                </tr>
                            ))}
                  </tbody>
            </table>
            </div>
          
            <div className='menufooter'>
            <Footer />
            </div>
           
        </div>
        </>
    );
}

export default MessMenu;
