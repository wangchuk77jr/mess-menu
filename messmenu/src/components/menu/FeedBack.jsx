import React, { useState } from 'react';
import "./Feedback.css";
import Footer from "../bar/Footer";
import TopNav from '../bar/TopNav';
import axios from 'axios';
import { toast,ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const FeedBack = () => {
  // State variables to store form data
  const [name, setName] = useState('');
  const [subject, setSubject] = useState('');
  const [message, setMessage] = useState('');

  // Function to handle form submission
  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post('http://localhost:5000/api/postFeedback', { name, subject, message });
      
      if (response.status === 201) {
        // Reset form fields after successful submission
        setName('');
        setSubject('');
        setMessage('');
        toast.success('Your feedback has been successfully submitted');
      }
    } catch (error) {
      if (error.response) {
        toast.error(`Error: ${error.response.data.error}`);
      } else {
        toast.error('Internal server error');
      }
    }
  };

  return (
    <>
    <div className='feedback'>
      <TopNav/>
      <h1 className='h5'>Do you have any feedback? Just inquire!</h1>
      <form onSubmit={handleSubmit} className="feedback-form">
        <div>
          <label htmlFor="name">Name:</label>
          <input
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
        </div>

        <div>
          <label htmlFor="subject">Subject:</label>
          <input
            type="text"
            value={subject}
            onChange={(e) => setSubject(e.target.value)}
            required
          />
        </div>

        <div>
          <label htmlFor="message">Message:</label>
          <textarea
            value={message}
            onChange={(e) => setMessage(e.target.value)}
            required
          />
        </div>

        <button type="submit">
          Submit
        </button>
      </form>
      <div className="feedbackfooter">
        <Footer />
      </div>
    </div>
    <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
                style={{zIndex:'9999999'}}
            />
    </>
  );
};

export default FeedBack;
