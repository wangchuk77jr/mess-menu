import React, { useState, useEffect } from "react";
import background from "../Assets/background.png";
import herobanner from '../Assets/herobanner.png';
import logo from '../Assets/logo.png';
import image1 from '../Assets/dish1.jpg'; 
import image2 from '../Assets/dish2.jpg';
import "./Home.css"
import { FaLongArrowAltDown } from "react-icons/fa";
import veg1 from '../Assets/veg1.png'
import veg2 from '../Assets/veg2.png'
import veg3 from '../Assets/veg3.png'
import veg4 from '../Assets/veg4.png'
import veg5 from '../Assets/veg5.png'
import veg6 from '../Assets/veg6.png'
import veg7 from '../Assets/veg7.png'
import veg8 from '../Assets/veg8.png'
import veg9 from '../Assets/veg9.png'
import nonveg1 from '../Assets/nonveg1.png'
import nonveg2 from '../Assets/nonveg2.png'
import nonveg3 from '../Assets/nonveg3.png'
import nonveg4 from '../Assets/nonveg4.png'
import nonveg5 from '../Assets/nonveg5.png'
import winter1 from '../Assets/winter1.png'
import winter2 from '../Assets/winter2.png'
import winter3 from '../Assets/winter3.png'
import summer1 from '../Assets/summer1.png'
import summer2 from '../Assets/summer2.png'
import Footer from "../bar/Footer";
import TopNav from "../bar/TopNav";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {useLocation} from "react-router-dom";

const Home = () => {
    const [specialMeals, setSpecialMeals] = useState([]);

    useEffect(() => {
        // Fetch special meals data from an API or other source
        // For now, using a mock data array
        const mockSpecialMeals = [
            { day: 'Wednesday', image: image1 },
            { day: 'Friday', image: image2 }
        ];
        setSpecialMeals(mockSpecialMeals);
    }, []);

    // message
    const location = useLocation();
    const successMessage = location.state && location.state.successMessage;
    useEffect(() => {
        if (successMessage) {
        toast.success(successMessage);
        }
    }, [successMessage]); // Trigger effect when successMessage changes

    return (

        <div className="about-section-container">
            <TopNav/>
            <img src={logo} alt="Logo" className="logo" />
            <div className="about-section-text-container">
                <p className="primary-subheading">GCIT <span className="us-text">MESS MENU DISPLAYER</span></p>
                <h1 className="primary-heading">“Discover Delicious Dining at GCIT's Mess Menu!”</h1>
            </div>
            <div className="savor">
            <p className="text">
          
                Savor every bite with our enticing array of freshly prepared dishes, 
                <br></br>
                thoughtfully crafted to delight your taste buds.
            </p>
            </div>
            <div className="about-section-image-container">
                <img src={herobanner} alt="" />
                <div className="about-background-image-container">
                    <img src={background} alt="" />
                </div>
            </div>
            <div>
            <div className="home-text">
                <p className="para-3">Get<span className="para-4"> special meals </span> <span className="para-5">on<FaLongArrowAltDown /></span></p>
            </div>
            <div className="card-container">
                {specialMeals.map((meal, index) => (
                    <div className="card" key={index}>
                        <img src={meal.image} alt={`Special Meal for ${meal.day}`} className="cardImg" />
                        <div className="cardBody">
                            <h5 className="cardTitle">{meal.day}</h5>
                        </div>
                    </div>
                ))}
            </div>
            {/* <div className="additional-line">
        <hr className="li"></hr>
      </div> */}
             <div>
             <p className="para-6"> Meals  <span className="para-7">  provided</span></p>
             <p className="category">Veg</p>
             </div>

            <div className="veg-container">
                <div className="veg-card">
                    {/* <div className="card"> */}
                        <img src={veg1} alt="Veg" className="cImg" />
                        <div className="cBody">
                            <h5 className="cTitle">Potato</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>
            <div className="veg-container2">
                <div className="veg-card2">
                    {/* <div className="card"> */}
                        <img src={veg2} alt="Veg2" className="cImg2" />
                        <div className="cBody2">
                            <h5 className="cTitle2">Mushroom</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>
            <div className="veg-container3">
                <div className="veg-card3">
                    {/* <div className="card"> */}
                        <img src={veg3} alt="Veg3" className="cImg3" />
                        <div className="cBody3">
                            <h5 className="cTitle3">Ema Datshi</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>
            <div className="veg-container4">
                <div className="veg-card4">
                    {/* <div className="card"> */}
                        <img src={veg4} alt="Veg4" className="cImg4" />
                        <div className="cBody4">
                            <h5 className="cTitle4">Cauliflower</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>
            <div className="veg-container5">
                <div className="veg-card5">
                    {/* <div className="card"> */}
                        <img src={veg5} alt="Veg5" className="cImg5" />
                        <div className="cBody5">
                            <h5 className="cTitle5">Cabbage</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>
            <div className="veg-container6">
                <div className="veg-card6">
                    {/* <div className="card"> */}
                        <img src={veg6} alt="Veg6" className="cImg6" />
                        <div className="cBody6">
                            <h5 className="cTitle6">Beans</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>
            <div className="veg-container7">
                <div className="veg-card7">
                    {/* <div className="card"> */}
                        <img src={veg7} alt="Veg7" className="cImg7" />
                        <div className="cBody7">
                            <h5 className="cTitle7">Fried cheese</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>
            <div className="veg-container8">
                <div className="veg-card8">
                    {/* <div className="card"> */}
                        <img src={veg8} alt="Veg8" className="cImg8" />
                        <div className="cBody8">
                            <h5 className="cTitle8">Spinach</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>
            <div className="veg-container9">
                <div className="veg-card9">
                    {/* <div className="card"> */}
                        <img src={veg9} alt="Veg9" className="cImg9" />
                        <div className="cBody9">
                            <h5 className="cTitle9">Brinjil</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>
            <div>
             <p className="non-veg-text1"> Non</p>
             <p className="non-veg-text2"> - Veg</p>
             </div>

             <div className="non-veg-container2">
                <div className="non-veg-card1">
                    {/* <div className="card"> */}
                        <img src={nonveg1} alt="nonVeg" className="nImg1" />
                        <div className="nBody">
                            <h5 className="nTitle9">Beef</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>


            <div className="non-veg-container2">
                <div className="non-veg-card2">
                    {/* <div className="card"> */}
                        <img src={nonveg2} alt="nonVeg" className="nImg1" />
                        <div className="nBody">
                            <h5 className="nTitle9">Chicken</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>

            <div className="non-veg-container3">
                <div className="non-veg-card3">
                    {/* <div className="card"> */}
                        <img src={nonveg3} alt="nonVeg" className="nImg3" />
                        <div className="nBody">
                            <h5 className="nTitle9">Pork</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>

            <div className="non-veg-container4">
                <div className="non-veg-card4">
                    {/* <div className="card"> */}
                        <img src={nonveg4} alt="nonVeg" className="nImg4" />
                        <div className="nBody">
                            <h5 className="nTitle9">Fish</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>

            <div className="non-veg-container5">
                <div className="non-veg-card5">
                    {/* <div className="card"> */}
                        <img src={nonveg5} alt="nonVeg" className="nImg5" />
                        <div className="nBody">
                            <h5 className="nTitle9">Egg</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>
            
            <div>
                <p className="weather-food">Grab our exclusive weather-aware offer</p>
                <h1 className="cta">Explore now!</h1>
                <p className="weather-winter">Winter:</p>
            </div>
            <p className="weather-summer">Summer:</p>
         
            <div className="weather-container">
                <div className="winter-card">
                    {/* <div className="card"> */}
                        <img src={winter1} alt="winter" className="wImg" />
                        <div className="wBody">
                            <h5 className="wTitle">Potato</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>

            <div className="weather-container2">
                <div className="winter-card2">
                    {/* <div className="card"> */}
                        <img src={winter2} alt="winter" className="wImg2" />
                        <div className="wBody1">
                            <h5 className="wTitle1">Potato</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>

            <div className="weather-container3">
                <div className="winter-card3">
                    {/* <div className="card"> */}
                        <img src={winter3} alt="winter" className="wImg3" />
                        <div className="wBody1">
                            <h5 className="wTitle1">Potato</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>

            <div className="container4">
                <div className="summer-card">
                    {/* <div className="card"> */}
                        <img src={summer1} alt="nonVeg" className="summerImg1" />
                        <div className="wBody">
                            <h5 className="wTitle">Fruits</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>

            <div className="container5">
                <div className="summer-card1">
                    {/* <div className="card"> */}
                        <img src={summer2} alt="nonVeg" className="summerImg2" />
                        <div className="wBody">
                            <h5 className="wTitle">Iced tea</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>   
        </div>
        <div className="homefooter">
        <Footer />
        </div>
        <ToastContainer
            position="top-center"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="colored"
            style={{zIndex:'9999999'}}
            />
        </div>
    );
};

export default Home;
