import React from "react";
import { Link, useLocation,useNavigate } from "react-router-dom";
import "./TopNav.css";

const TopNav = () => {
  const location = useLocation();

  // Function to check if the current route is the login or signup page
  const isLoginPage = location.pathname === "/login";
  const isSignupPage = location.pathname === "/signup";
  const isAdminLoginPage = location.pathname === "/gcitadminmenulogin";


  // Combine conditions to check if the current route is either login or signup page
  const hideNav = isLoginPage || isSignupPage || isAdminLoginPage ;

  const navigate = useNavigate();
  const handleLogout = () => {
    // Clear user authentication data from local storage
    localStorage.clear();
    // Navigate to the login page
    window.location.reload();
    navigate('/');
  };

  return (
    <div className={`top-nav ${hideNav ? 'hidden' : ''}`}>
      <div className="nav">
        <Link className="nav-link" to="/home">
          HOME
        </Link>

        <Link className="nav-link" to="/messmenu">
          MessMenu
        </Link>

        <Link className="nav-link" to="/feedback">
          FEEDBACK
        </Link>

        <Link className="nav-link" to="/about">
          ABOUT
        </Link>

        <Link className="nav-link" to="/notification">
          NOTIFICATION
        </Link>

        <Link onClick={handleLogout} className="nav-link">
          LOGOUT
        </Link>
      </div>
    </div>
  );
};

export default TopNav;
