import React from 'react';
import './Footer.css';
import phone from "../Assets/tele.png";
import mail from "../Assets/mail.png";
import location from "../Assets/location.png";
import logo from '../Assets/logowhite.png';
import c from '../Assets/c.png';
import { RiFacebookBoxFill } from "react-icons/ri";
import { FaInstagram } from "react-icons/fa6";
import { FaTwitter } from "react-icons/fa6";
import { BiLogoYoutube } from "react-icons/bi";


const Footer = () => {
    return (
        <React.Fragment>
            <div className="Footer">
                <div className="container">
                    <div className="row">
                        <div className="text1">
                            <h3>Get connect with us on social networks:</h3>
                            <div>
                            <img src={logo} alt="Logo" className="logo2" />
                            </div>
                        </div>
                       
                        <h1 className='text2'>Links</h1>
                            <ul>
                                <li className="nav-item">
                                    <a className="gcit" href="/">GCIT</a>
                                </li>
                            </ul>
                            <div className='contact'>
                                <h1>Contact</h1>
                              
                            </div>
                            <p className='number'>
                                17656789
                                </p>
                                <p className='mail'>
                                GCITMess@gmail.com
                                </p>
                                
                                <h1 className='location'>
                                Location
                                </h1>

                                <p className='location2'>
                                Kabesa, Thimphu
                                </p>

                                <p className='lasttext'>
                                GCIT-MESS-Management 2024. All Rights Reserved.
                                </p>
                        </div>
                         <div className='image1'>
                        <img src={phone} alt="" className="phone" />
                        </div>

                        <div className='image2'>
                        <img src={mail} alt="" className="mail2" />
                        </div>

                        <div className='image3'>
                        <img src={location} alt="" className="location11" />
                        </div>

                        <div className='image4'>
                        <img src={c} alt="" className="c" />
                        </div>
     
      <div className='media1'>
      <RiFacebookBoxFill />
      </div>

      <div className='media2'>
      <FaInstagram />
      </div>

      <div className='media3'>
      <FaTwitter />
      </div>

      <div className='media4'>
      <BiLogoYoutube />
      </div>
            
                    </div>
                </div>
          
        
        </React.Fragment>
    )
}

export default Footer;
