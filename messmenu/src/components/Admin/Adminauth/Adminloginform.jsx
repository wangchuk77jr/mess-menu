// import React from 'react';
import React, { useState } from 'react';
import'./Adminlogin.css';
import Food from '../../Assets/pic1.png';
import Food2 from '../../Assets/pic2.jpeg';
import Food3 from '../../Assets/pic3.jpg';
import logo from '../../Assets/logo.png';
import { useNavigate } from "react-router-dom";

   


const AdminLogin = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [emailError, setEmailError] = useState('');
    const [passwordError, setPasswordError] = useState('');
    const navigate = useNavigate();
  

    const handleEmailChange = (e) => {
        setEmail(e.target.value);
    };

    const handlePasswordChange = (e) => {
        setPassword(e.target.value);
    };

    const validateEmail = () => {
        const regex = /^[0-9]{8}\.[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        if (!regex.test(email)) {
            setEmailError('Please enter a valid email address');
        } else {
            setEmailError('');
        }
    };

    const validatePassword = () => {
        if (password.length < 8) {
            setPasswordError('! Password must be at least 8 characters long');
        } else {
            setPasswordError('');
        }
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        validateEmail();
        validatePassword();
      
           // Check if there are any validation errors
    if (!emailError && !passwordError) {
        // If no validation errors, submit the form
        // You can perform additional actions here, such as sending form data to the server
        console.log('Form submitted successfully');
        // Reset the form fields
        setEmail('');
        setPassword('');
        // history.push('/');
        navigate("/gcitadminmenuhome");

    } else {
        
        console.log('Form contains errors. Please fix them.');
       
    }

    };

    return (
        <div className='wrapper'>
            <img src={Food} alt="Food" className="food-image" />
            <img src={Food2} alt="Food" className="food-image2" />
            <img src={Food3} alt="Food" className="food-image3" />
            <img src={logo} alt="Food" className="logo" />
            <form action="">
                <h1>LOGIN TO YOUR ACCOUNT</h1>
                <br></br>
                <hr></hr>
                <br></br>
                <div className="input-box">
                    <input type="text" placeholder="example.gcit@rub.edu.bt" required   value={email}
        onChange={handleEmailChange}/>
        {emailError && <p className="error-message">{emailError}</p>}
                                      
                    <br></br>
                </div>
                
                <div className="input-box">
                    <input type="password" placeholder='Password' required value={password}
        onChange={handlePasswordChange}/>
                    {passwordError && <p className="error-message">{passwordError}</p>}
                </div>

                <button type="input-box"  onClick={handleSubmit}>LOGIN</button>
              
            </form>
        </div>
    );
};

export default AdminLogin;
