import React from 'react';
import { Link } from 'react-router-dom'; // Import Link from react-router-dom

const AdminLogout = () => {
    const handleLogout = () => {
        // Display a confirmation dialog
        const confirmLogout = window.confirm('Are you sure you want to logout?');
        if (confirmLogout) {
            // Perform logout action and redirect to login page
            // Example:
            // localStorage.removeItem('token'); // Clear token from local storage
            // window.location.href = '/login'; // Redirect to login page
        } else {
            // Do nothing, user chose to stay on logout page
        }
    };

    return (
        <div className="container-fluid h1 p5 text-center">
            {/* Use Link component to navigate to login page */}
            <Link to="/login" onClick={handleLogout}>
                LOGOUT
            </Link>
        </div>
    );
};

export default AdminLogout;
