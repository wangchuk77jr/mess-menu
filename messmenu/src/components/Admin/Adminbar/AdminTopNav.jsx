import React from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import "./AdminTopNav.css";

const AdminTopNav = () => {
  const location = useLocation();
  
  const navigate = useNavigate();
  const handleLogout = () => {
    // Clear user authentication data from local storage
    localStorage.clear();
    // Navigate to the login page
    window.location.reload();
    navigate('/');
  };

  // Function to check if the current route is the admin login page
  const isAdminLoginPage = location.pathname === "/gcitadminmenulogin";

  // Conditionally apply the 'hidden' class to hide the navigation bar on the login page
  const navClass = isAdminLoginPage ? 'top-nav hidden' : 'top-nav';

  return (
    <div className={navClass}>
      <div className="nav">
        <Link className="nav-link" to="/gcitadminmenuhome">
          HOME
        </Link>

        <Link className="nav-link" to="/gcitadminmenumessmenu">
          MessMenu
        </Link>

        <Link className="nav-link" to="/gcitadminmenufeedback">
          FEEDBACK
        </Link>

        <Link className="nav-link" to="/gcitadminmenunotification">
          NOTIFICATION
        </Link>

        <Link className="nav-link" onClick={handleLogout}>
          LOGOUT
        </Link>
      </div>
    </div>
  );
};

export default AdminTopNav;
