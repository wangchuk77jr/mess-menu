import React, { useState, useEffect } from 'react';
import "./Adminmessmenu.css";
import mess from "../../Assets/mess.png";
import Footer from "../Adminbar/AdminFooter";
import background from "../../Assets/background.png";
import AdminTopNav from "../Adminbar/AdminTopNav";
import axios from 'axios';
import { toast,ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const AdminMessMenu = () => {
    const [todaysMenu, setTodaysMenu] = useState([]);
    const [LateClassesMenu, setLateClassesMenu] = useState([]);
    const [editMenu, setEditMenu] = useState(null);

    useEffect(() => {
        const fetchTodaysMenu = async () => {
            try {
                const response = await axios.get('http://localhost:5000/api/getTodaysMenu');
                setTodaysMenu(response.data);
            } catch (error) {
                console.error('Error fetching today\'s menu:', error);
            }
        };
        fetchTodaysMenu();
    }, []);

    useEffect(() => {
        const fetchLateClassesMenu = async () => {
            try {
                const response = await axios.get('http://localhost:5000/api/getLateClassesMenu');
                setLateClassesMenu(response.data);
            } catch (error) {
                console.error('Error fetching late classes menu:', error);
            }
        };
        fetchLateClassesMenu();
    }, []);

    const handleEditButtonClick = (menu) => {
        setEditMenu(menu);
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setEditMenu(prevMenu => ({
            ...prevMenu,
            [name]: value
        }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.put('http://localhost:5000/api/updateMenu', editMenu);
            if (response.data.success) {
                console.log('Menu item updated successfully');
                // Update todaysMenu state to reflect changes
                setTodaysMenu(prevMenu => prevMenu.map(item => (item.id === editMenu.id ? editMenu : item)));
                toast.success('You have updated menu successfully');
            } else {
                console.error(response.data.message);
                toast.error(response.data.message);
            }
        } catch (error) {
            console.error('Failed to update menu item:', error);
            toast.error('Failed to update menu item:',error);
        }
    };

    return (
        <>
            <AdminTopNav/>
            <div className="about-background-image">
                <img src={background} alt="" />
            </div>
            <div className="mess-text-container">
                <h1 className="mess-heading">Our <span className='mess-heading2'>menu</span></h1>
                <img src={mess} alt="mess" className="mess-herobanner"/>
                <div className='mess-text'>
                    <p className="mess-para1">Welcome to our menu page! Our menu offers a delightful array of culinary creations </p>
                    <p className="mess-para2">designed to tantalize your taste buds and satisfy your cravings. From hearty breakfast</p>
                    <p className="mess-para3">  options to mouthwatering lunch and dinner selections, we have something </p>
                    <p className="mess-para4"> to suit every palate.</p>
                </div>
            </div>
            <div className="whole-table">
                <h1 className='m1'>Todays menu</h1>
                <table className="menu-table">
                    <thead>
                        <tr className="Head">
                            <th>Day</th>
                            <th>Breakfast (7:00 a.m. - 9:00 a.m.)</th>
                            <th>Lunch (12:00 p.m. - 2:00 p.m.)</th>
                            <th>Dinner (7:15 p.m. - 8:00 p.m.)</th>
                            <th>(Actions)</th>
                        </tr>
                    </thead>
                    <tbody>
                        {todaysMenu.map(menu => (
                            <tr key={menu.id}>
                                <td>{menu.dayname}</td>
                                <td>{menu.breakfast || ""}</td>
                                <td>{menu.lunch || ""}</td>
                                <td>{menu.dinner || ""}</td>
                                <td>
                                    <button type='button' data-bs-toggle="modal" data-bs-target={`#todayMenuModal${menu.id}`} onClick={() => handleEditButtonClick(menu)}>
                                        Edit
                                    </button>
                                    <div className="modal fade" id={`todayMenuModal${menu.id}`} tabIndex="-1" aria-labelledby="todayMenuModalLabel" aria-hidden="true">
                                        <div className="modal-dialog modal-lg modal-dialog-centered">
                                            <form className="modal-content" onSubmit={handleSubmit}>
                                                <div className="modal-header">
                                                    <h1 className="modal-title fs-5" id="todayMenuModalLabel">Edit Menu for {menu.dayname}</h1>
                                                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div className="modal-body">
                                                    <input type="hidden" name="id" value={editMenu?.id} readOnly />
                                                    <div className="mb-3">
                                                        <label className="form-label">Breakfast</label>
                                                        <input required type="text" className="form-control" name="breakfast" value={editMenu?.breakfast || ""} onChange={handleInputChange} />
                                                    </div>
                                                    <div className="mb-3">
                                                        <label className="form-label">Lunch</label>
                                                        <input required type="text" className="form-control" name="lunch" value={editMenu?.lunch || ""} onChange={handleInputChange} />
                                                    </div>
                                                    <div className="mb-3">
                                                        <label className="form-label">Dinner</label>
                                                        <input required type="text" className="form-control" name="dinner" value={editMenu?.dinner || ""} onChange={handleInputChange} />
                                                    </div>
                                                </div>
                                                <div className="modal-footer">
                                                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                    <button type="submit" style={{ background: '#e1680b' }} className="btn btn-primary">Save Changes</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <div className='second-table'>
                    <h1 className="mt-5">For late classes</h1>
                    <table className="menu-table">
                        <thead>
                            <tr className="Head"> 
                                <th>Day</th>
                                <th>Breakfast (8:30 a.m. - 9:00 a.m.)</th>
                                <th>Lunch (2:00 p.m. - 3:00 p.m.)</th>
                                <th>Dinner (8:00 p.m. - 8:30 p.m.)</th>
                                <th>(Actions)</th>
                            </tr>
                        </thead>
                        <tbody>
                            {LateClassesMenu.map(menu => (
                                <tr key={menu.id}>
                                    <td>{menu.dayname}</td>
                                    <td>{menu.breakfast || ""}</td>
                                    <td>{menu.lunch || ""}</td>
                                    <td>{menu.dinner || ""}</td>
                                    <td>
                                        <button type='button' data-bs-toggle="modal" data-bs-target={`#todayMenuModal${menu.id}`} onClick={() => handleEditButtonClick(menu)}>
                                            Edit
                                        </button>
                                        <div className="modal fade" id={`todayMenuModal${menu.id}`} tabIndex="-1" aria-labelledby="todayMenuModalLabel" aria-hidden="true">
                                            <div className="modal-dialog modal-lg modal-dialog-centered">
                                                <form className="modal-content" onSubmit={handleSubmit}>
                                                    <div className="modal-header">
                                                        <h1 className="modal-title fs-5" id="todayMenuModalLabel">Edit Menu for {menu.dayname}</h1>
                                                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div className="modal-body">
                                                        <input type="hidden" name="id" value={editMenu?.id} readOnly />
                                                        <div className="mb-3">
                                                            <label className="form-label">Breakfast</label>
                                                            <input required type="text" className="form-control" name="breakfast" value={editMenu?.breakfast || ""} onChange={handleInputChange} />
                                                        </div>
                                                        <div className="mb-3">
                                                            <label className="form-label">Lunch</label>
                                                            <input required type="text" className="form-control" name="lunch" value={editMenu?.lunch || ""} onChange={handleInputChange} />
                                                        </div>
                                                        <div className="mb-3">
                                                            <label className="form-label">Dinner</label>
                                                            <input required type="text" className="form-control" name="dinner" value={editMenu?.dinner || ""} onChange={handleInputChange} />
                                                        </div>
                                                    </div>
                                                    <div className="modal-footer">
                                                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                        <button type="submit" style={{ background: '#e1680b' }} className="btn btn-primary">Save Changes</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
                <div className='menufooter'>
                    <Footer />
                </div>
            </div>
            <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
                style={{zIndex:'9999999'}}
            />
        </>
    );
}

export default AdminMessMenu;
