
import React, { useState, useEffect } from "react";
import background from "../../Assets/background.png";
import herobanner from '../../Assets/herobanner.png';
import logo from '../../Assets/logo.png';
import image1 from '../../Assets/dish1.jpg'; 
import image2 from '../../Assets/dish2.jpg';
import "./Adminhome.css"
import { FaLongArrowAltDown } from "react-icons/fa";
import { toast,ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Footer from "../Adminbar/AdminFooter";
import AdminTopNav from "../Adminbar/AdminTopNav";
import axios from "axios";

const AdminHome = () => {
    const [specialMeals, setSpecialMeals] = useState([]);

    useEffect(() => {
        // Fetch special meals data from an API or other source
        // For now, using a mock data array
        const mockSpecialMeals = [
            { day: 'Wednesday', image: image1 },
            { day: 'Friday', image: image2 }
        ];
        setSpecialMeals(mockSpecialMeals);
    }, []);


  const [mealCategory, setMealCategory] = useState('');
  const [mealName, setMealName] = useState('');
  const [mealImage, setMealImage] = useState(null);
  const [errorMessage, setErrorMessage] = useState('');
  const [vegMeal, setVegMeal] = useState([]);
  const [nonvegMeal, setNonVegMeal] = useState([]);
  const [summerMeal, setSummerMeal] = useState([]);
  const [winterMeal, setWinterMeal] = useState([]);

  useEffect(() => {
    const fetchMeals = async () => {
      try {
        const response = await axios.get('http://localhost:5000/api/getMeals');

        const meals = response.data.meals;
        const vegMeals = meals.filter(meal => meal.mealcategory === 'veg');
        const nonVegMeals = meals.filter(meal => meal.mealcategory === 'non_veg');
        const summerMeals = meals.filter(meal => meal.mealcategory === 'summer');
        const winterMeals = meals.filter(meal => meal.mealcategory === 'winter');

        setVegMeal(vegMeals);
        setNonVegMeal(nonVegMeals);
        setSummerMeal(summerMeals);
        setWinterMeal(winterMeals);
      } catch (error) {
        console.error('Error fetching meals:', error);
      }
    };

    fetchMeals();
  }, []);

  const handleButtonClick = (category) => {
    setMealCategory(category);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    // Create form data to send meal data and image
    const formData = new FormData();
    formData.append('mealCategory', mealCategory);
    formData.append('mealName', mealName);
    formData.append('image', mealImage);

    try {
      // Make POST request to backend
      const response = await axios.post('http://localhost:5000/api/postMeals', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });

      // Display success message
      toast.success(response.data.message);
      // Clear form fields after successful submission
      setMealName('');
      setMealImage(null);
      setErrorMessage('');
    } catch (error) {
      // Display error message if request fails
      setErrorMessage('Failed to create meal. Please try again.');
      toast.error('Error creating meal:', error);
    }
  };

    return (
      <>
        <div className="about-section-container">
            <AdminTopNav/>

            <img src={logo} alt="Logo" className="logo" />
            <div className="about-section-text-container">
                <p className="primary-subheading">GCIT <span className="us-text">MESS MENU DISPLAYER</span></p>
                <h1 className="primary-heading">“Discover Delicious Dining at GCIT's Mess Menu!”</h1>
            </div>
            <div className="savor">
            <p className="text">
          
                Savor every bite with our enticing array of freshly prepared dishes, 
                <br></br>
                thoughtfully crafted to delight your taste buds.
            </p>
            </div>
            <div className="about-section-image-container">
                <img src={herobanner} alt="" />
                <div className="about-background-image-container">
                    <img src={background} alt="" />
                </div>
            </div>
            <div>
            <div className="home-text">
                <p className="para-3">Get<span className="para-4"> special meals </span> <span className="para-5">on<FaLongArrowAltDown /></span></p>
            </div>
            <div className="card-container">
                {specialMeals.map((meal, index) => (
                    <div className="card" key={index}>
                        <img src={meal.image} alt={`Special Meal for ${meal.day}`} className="cardImg" />
                        <div className="cardBody">
                            <h5 className="cardTitle">{meal.day}</h5>
                        </div>
                    </div>
                ))}
            </div>
            {/* <div className="additional-line">
        <hr className="li"></hr>
      </div> */}
             <div>
             <p className="para-6"> Meals  <span className="para-7">  provided</span></p>
             <p className="category">Veg</p>
             </div>
             <div className="bg-danger">
           {vegMeal.map(meal => (
                <div key={meal.id} className="veg-container">
                    <div className="veg-card">
                        {/* <div className="card"> */}
                            <img src={`http://localhost:5000/uploads/${encodeURIComponent(meal.mealimage)}`} alt="Veg" className="cImg" />
                            <div className="cBody">
                                <h5 className="cTitle">{meal.mealname}</h5>
                            </div>
                        {/* </div> */}
                    </div>
                </div>  
            ))}
             </div>
            <div>
             <p className="non-veg-text1"> Non</p>
             <p className="non-veg-text2"> - Veg</p>
             </div>
             {nonvegMeal.map(meal => (
                <div key={meal.id} className="non-veg-container2">
                    <div className="non-veg-card1">
                        {/* <div className="card"> */}
                            <img src={`http://localhost:5000/uploads/${encodeURIComponent(meal.mealimage)}`} alt="nonVeg" className="nImg1" />
                            <div className="nBody">
                                <h5 className="nTitle9">{meal.mealname}</h5>
                            </div>
                        {/* </div> */}
                    </div>
                </div>
            ))}

            
            <div>
                <p className="weather-food">Grab our exclusive weather-aware offer</p>
                <h1 className="cta">Explore now!</h1>
                <p className="weather-winter">Winter:</p>
            </div>
            <p className="weather-summer">Summer:</p>
         

            {summerMeal.map(meal => (
                <div  key={meal.id} className="weather-container">
                    <div className="winter-card">
                        {/* <div className="card"> */}
                            <img src={`http://localhost:5000/uploads/${encodeURIComponent(meal.mealimage)}`} alt="winter" className="wImg" />
                            <div className="wBody">
                                <h5 className="wTitle">{meal.mealname}</h5>
                            </div>
                        {/* </div> */}
                    </div>
                </div>
            ))}




            <div className="button-container">
                 <button className="button" onClick={() => handleButtonClick('winter')} data-bs-toggle="modal" data-bs-target=".uplodMeals">Upload+</button>
            </div>


            {winterMeal.map(meal => (
              <div key={meal.id} className="container5">
                <div className="summer-card1">
                    {/* <div className="card"> */}
                        <img src={`http://localhost:5000/uploads/${encodeURIComponent(meal.mealimage)}`} alt="nonVeg" className="summerImg2" />
                        <div className="">
                            <h5 className="wTitle">{meal.mealname}</h5>
                        </div>
                    {/* </div> */}
                </div>
            </div>  
            ))}
 
        </div>


        <div className="button-container1">
        <button className="button1" onClick={() => handleButtonClick('summer')} data-bs-toggle="modal" data-bs-target=".uplodMeals">Upload+</button>

      <div className="modal fade uplodMeals" tabIndex="-1" aria-labelledby="todayMenuModalLabel" aria-hidden="true">
        <div className="modal-dialog modal-lg modal-dialog-centered">
          <form className="modal-content" onSubmit={handleSubmit}>
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="todayMenuModalLabel">Upload Meals {mealCategory.toUpperCase()}</h1>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <div className="mb-3">
                <label className="form-label">Meal Name</label>
                <input required type="text" className="form-control" name="name" value={mealName} onChange={(e) => setMealName(e.target.value)} />
              </div>
              <div className="mb-3">
                <label className="form-label">Meal Image</label>
                <input required type="file" className="form-control" onChange={(e) => setMealImage(e.target.files[0])} />
              </div>
            </div>
            {errorMessage && <div className="text-danger">{errorMessage}</div>}
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="submit" style={{ background: '#e1680b' }} className="btn btn-primary">Save Changes</button>
            </div>
          </form>
        </div>
      </div>
        </div>

        <div className="homefooter">
        <Footer />
        </div>
        <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
                style={{zIndex:'9999999'}}
            />

        </div>
        </>
    );
};

export default AdminHome;


// import React from 'react'

// const Adminhome = () => {
//   return (
//     <div>
//       <nav class="navbar navbar-expand-lg bg-primary">
//         <div class="container-fluid">
//           <a class="navbar-brand" href="#">Navbar</a>
//           <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
//             <span class="navbar-toggler-icon"></span>
//           </button>
//           <div class="collapse navbar-collapse" id="navbarSupportedContent">
//             <ul class="navbar-nav me-auto mb-2 mb-lg-0">
//               <li class="nav-item">
//                 <a class="nav-link active" aria-current="page" href="#">Home</a>
//               </li>
//               <li class="nav-item">
//                 <a class="nav-link" href="#">Link</a>
//               </li>
//               <li class="nav-item dropdown">
//                 <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
//                   Dropdown
//                 </a>
//                 <ul class="dropdown-menu">
//                   <li><a class="dropdown-item" href="#">Action</a></li>
//                   <li><a class="dropdown-item" href="#">Another action</a></li>
//                   <li><hr class="dropdown-divider"/></li>
//                   <li><a class="dropdown-item" href="#">Something else here</a></li>
//                 </ul>
//               </li>
//               <li class="nav-item">
//                 <a class="nav-link disabled" aria-disabled="true">Disabled</a>
//               </li>
//             </ul>
//             <form class="d-flex" role="search">
//               <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search"/>
//               <button class="btn btn-outline-success" type="submit">Search</button>
//             </form>
//           </div>
//         </div>
//       </nav>
//       {/*  */}
//       <section>
//         asas
//       </section>
//     </div>
//   )
// }

// export default Adminhome

