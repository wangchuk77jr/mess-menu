import React ,{useEffect,useState} from 'react';
import axios from 'axios';
import StudentsLayout from '../../layouts/StudentsLayout'
import './Student.css';

const MessMenu = () => {
    const [todaysMenu, setTodaysMenu] = useState([]);
    const [LateClassesMenu, setLateClassesMenu] = useState([]);

    useEffect(() => {
        const fetchTodaysMenu = async () => {
            try {
                const response = await axios.get('http://localhost:5000/api/getTodaysMenu');
                setTodaysMenu(response.data);
            } catch (error) {
                console.error('Error fetching today\'s menu:', error);
            }
        };
        fetchTodaysMenu();
    }, []);

    useEffect(() => {
        const fetchLateClassesMenu = async () => {
            try {
                const response = await axios.get('http://localhost:5000/api/getLateClassesMenu');
                setLateClassesMenu(response.data);
            } catch (error) {
                console.error('Error fetching late classes menu:', error);
            }
        };
        fetchLateClassesMenu();
    }, []);
   
    return (
        <StudentsLayout>
            {/* herobanner Section */}
            <section style={{boxShadow: '0px 5px 5px 0px rgba(0, 0, 0, 0.509)'}} className='container-fluid px-md-5 px-3'>
                <div className="herobanner-menu-section mt-3 d-flex flex-column justify-content-center gap-4">
                    <span>
                        <span className='text-primary fs-1 fw-bold'>Our</span>
                        <span className='fs-1 fw-bold ps-2'>menu</span>
                    </span>
                    <h5 className='w-50'>Welcome to our menu page! Our menu offers a delightful array of culinary creations designed to tantalize your taste buds and satisfy your cravings. From hearty breakfast options to mouthwatering lunch  </h5>
                </div>
            </section>
            <div className="container-fluid px-md-5 px-3 mb-5 table-responsive">
                <h5 className='mt-5'>Todays menu</h5>
                <table className="menu-table">
                    <thead>
                        <tr className="Head">
                            <th>Day</th>
                            <th>Breakfast (7:00 a.m. - 9:00 a.m.)</th>
                            <th>Lunch (12:00 p.m. - 2:00 p.m.)</th>
                            <th>Dinner (7:15 p.m. - 8:00 p.m.)</th>
                        </tr>
                    </thead>
                    <tbody>
                    {todaysMenu.map(menu => (
                                <tr key={menu.id}>
                                    <td>{menu.dayname}</td>
                                    <td>{menu.breakfast || ""}</td>
                                    <td>{menu.lunch || ""}</td>
                                    <td>{menu.dinner || ""}</td>
                                </tr>
                            ))}
                    </tbody>
                </table>
                <div className='second-table'>
                <h5 className="mt-5">For late classes</h5>
                <table className="menu-table">
                    <thead>
                        <tr className="Head"> 
                            <th>Day</th>
                            <th>Breakfast (8:30 a.m. - 9:00 a.m.)</th>
                            <th>Lunch (2:00 p.m. - 3:00 p.m.)</th>
                            <th>Dinner (8:00 p.m. - 8:30 p.m.)</th>
                        </tr>
                    </thead>
                            <tbody>
                                {LateClassesMenu.map(menu => (
                                    <tr key={menu.id}>
                                        <td>{menu.dayname}</td>
                                        <td>{menu.breakfast || ""}</td>
                                        <td>{menu.lunch || ""}</td>
                                        <td>{menu.dinner || ""}</td>
                                    </tr>
                                ))}
                    </tbody>
                </table>
                </div>    
            </div>
        </StudentsLayout>
    );
}

export default MessMenu;
