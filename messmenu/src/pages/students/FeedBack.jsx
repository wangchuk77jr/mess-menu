import React, { useState } from 'react';
import axios from 'axios';
import { toast,ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import StudentsLayout from '../../layouts/StudentsLayout'

const FeedBack = () => {
  // State variables to store form data
  const [name, setName] = useState('');
  const [subject, setSubject] = useState('');
  const [message, setMessage] = useState('');

  // Function to handle form submission
  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post('http://localhost:5000/api/postFeedback', { name, subject, message });
      
      if (response.status === 201) {
        // Reset form fields after successful submission
        setName('');
        setSubject('');
        setMessage('');
        toast.success('Your feedback has been successfully submitted');
      }
    } catch (error) {
      if (error.response) {
        toast.error(`Error: ${error.response.data.error}`);
      } else {
        toast.error('Internal server error');
      }
    }
  };

  return (
    <StudentsLayout>
    <div className='container mt-4 d-flex flex-column justify-content-center align-items-center'>
      <form onSubmit={handleSubmit} className='d-flex flex-column gap-2 col-md-4 p-3 rounded' style={{background:'#DDDEC4'}}>
        <h5 className='text-center'>Do you have any feedback? Just inquire!</h5>
        <div className='mt-4'>
          <label htmlFor="name">Name:</label>
          <input
            style={{background:'#F0F1D7'}}
            className=' form-control'
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
        </div>

        <div>
          <label htmlFor="subject">Subject:</label>
          <input
            style={{background:'#F0F1D7'}}
            className=' form-control'
            type="text"
            value={subject}
            onChange={(e) => setSubject(e.target.value)}
            required
          />
        </div>

        <div>
          <label htmlFor="message">Message:</label>
          <textarea
            style={{background:'#F0F1D7'}}
            rows="8"
            className=' form-control'
            value={message}
            onChange={(e) => setMessage(e.target.value)}
            required
          />
        </div>

        <button className='btn btn-outline-dark mt-5' type="submit">
          Submit
        </button>
      </form>
    </div>
    <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
                style={{zIndex:'9999999'}}
            />
    </StudentsLayout>
  );
};

export default FeedBack;
