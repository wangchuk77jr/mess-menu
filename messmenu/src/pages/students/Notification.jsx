import React, {useState,useEffect} from "react";
import axios from "axios";
import StudentsLayout from '../../layouts/StudentsLayout'

const Notification = () => {
  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    // Fetch notifications when the component mounts
    const fetchNotifications = async () => {
      try {
        const response = await axios.get("http://localhost:5000/api/notifications");
        setNotifications(response.data);
      } catch (error) {
        console.error("Error fetching notifications:", error);
      }
    };
    fetchNotifications();
  }, []); // Empty dependency array to run effect only once on mount

    return (
      <StudentsLayout>
          <div className="container-fluid px-md-5 px-3 mt-5">
          {notifications.map((n, index) => (
            <div style={{boxShadow: 'rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px'}} key={index} className="w-100 bg-body-secondary p-3 rounded gap-2 mb-3">
              (Notification)
              <h5 className="mt-3">Subject: {n.subject}</h5>
              <h5>Message: {n.message}</h5>
            </div>
          ))}
        </div>
    
      </StudentsLayout>
    );
  };

export default Notification;
