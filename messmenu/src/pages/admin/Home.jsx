import React,{useState,useEffect} from 'react'
import AdminLayout from '../../layouts/AdminLayout';
import './Admin.css';
import Special1 from '../../assets/dish1.jpg';
import Special2 from '../../assets/dish2.jpg';
import axios from "axios";
import { toast,ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Home = () => {
  const [vegMeal, setVegMeal] = useState([]);
  const [nonvegMeal, setNonVegMeal] = useState([]);
  const [summerMeal, setSummerMeal] = useState([]);
  const [winterMeal, setWinterMeal] = useState([]);

    
  const [mealCategory, setMealCategory] = useState('');
  const [mealName, setMealName] = useState('');
  const [mealImage, setMealImage] = useState(null);
  const [errorMessage, setErrorMessage] = useState('');

  const handleButtonClick = (category) => {
    setMealCategory(category);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    // Create form data to send meal data and image
    const formData = new FormData();
    formData.append('mealCategory', mealCategory);
    formData.append('mealName', mealName);
    formData.append('image', mealImage);

    try {
      // Make POST request to backend
      const response = await axios.post('http://localhost:5000/api/postMeals', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });

      // Display success message
      toast.success(response.data.message);
      // Clear form fields after successful submission
      setMealName('');
      setMealImage(null);
      setErrorMessage('');
    } catch (error) {
      // Display error message if request fails
      setErrorMessage('Failed to create meal. Please try again.');
      toast.error('Error creating meal:', error);
    }
  };


    useEffect(() => {
        const fetchMeals = async () => {
          try {
            const response = await axios.get('http://localhost:5000/api/getMeals');
    
            const meals = response.data.meals;
            const vegMeals = meals.filter(meal => meal.mealcategory === 'veg');
            const nonVegMeals = meals.filter(meal => meal.mealcategory === 'non_veg');
            const summerMeals = meals.filter(meal => meal.mealcategory === 'summer');
            const winterMeals = meals.filter(meal => meal.mealcategory === 'winter');
    
            setVegMeal(vegMeals);
            setNonVegMeal(nonVegMeals);
            setSummerMeal(summerMeals);
            setWinterMeal(winterMeals);
          } catch (error) {
            console.error('Error fetching meals:', error);
          }
        };
    
        fetchMeals();
      }, []);
    
  return (
    <AdminLayout>
        {/* herobanner Section */}
        <section style={{boxShadow: '0px 5px 5px 0px rgba(0, 0, 0, 0.509)'}} className='container-fluid px-md-5 px-3'>
            <div className="herobanner-section mt-3 d-flex flex-column justify-content-center gap-4">
                <span>
                    <span style={{fontSize:'80px'}} className='text-primary fw-bolder'>GCIT</span>
                    <span className='fs-1 fw-bold ps-2'>MESS MENU DISPLAYER</span>
                </span>
                <h5>“Discover Delicious Dining at GCIT's Mess Menu!”</h5>
                <h5 className='w-50'>Savor every bite with our enticing array of freshly prepared dishes, thoughtfully crafted to delight your taste buds</h5>
            </div>
        </section>
        {/* Specail Meal Section */}
        <section className='container-fluid px-md-5 px-3 mt-5'>
            <h1 className='text-center w-100 mb-4 d-flex justify-content-center align-items-center gap-2'>Get <span className='text-primary'>special meal’s</span> on <box-icon size="lg" name='down-arrow-alt' color='#e1680b' ></box-icon></h1>
            <div className="row">
                <div className="col-md-6">
                    <div style={{background:'#DD7D25'}}>
                        <img style={{objectFit:'cover',width:'100%',height:'300px'}} src={Special1} alt="" />
                        <h2 className='text-center text-white py-2'>Tuesday</h2>
                    </div>
                </div>
                <div className="col-md-6">
                    <div style={{background:'#DD7D25'}}>
                        <img style={{objectFit:'cover',width:'100%',height:'300px'}} src={Special2} alt="" />
                        <h2 className='text-center text-white py-2'>Friday</h2>
                    </div>
                </div>
            </div>
            <hr className='my-5' style={{border: '1px solid #000'}} />
        </section>
        {/* Meals Provided Section */}
        <section className='container-fluid px-md-5 px-3 mt-5'>
            <h1 className='text-center w-100 mb-4 d-flex justify-content-center align-items-center gap-2'>Meals  <span className='text-primary'>provided</span></h1>
            {/* Veg Meal Section */}
            <h2 className='text-success mt-3 mb-5'>Veg</h2>
            <div className="row g-5">
                {vegMeal.map(meal => (
                    <div key={meal.id} className="col-md-4">
                        <div style={{boxShadow: 'rgba(0, 0, 0, 0.3) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px'}}>
                            <img style={{objectFit:'cover',width:'100%',height:'200px'}} src={`http://localhost:5000/uploads/${encodeURIComponent(meal.mealimage)}`} alt="img veg" />
                            <h6 className='text-center py-2 text-capitalize'>{meal.mealname}</h6>
                        </div>
                    </div>  
                ))}
            </div>
            {/* Upload Button */}
            <div className="row mb-4">
                <div className='w-100'>
                     <button className="btn d-block ms-auto btn-outline-primary me-5" onClick={() => handleButtonClick('veg')} data-bs-toggle="modal" data-bs-target=".uplodMeals">Upload+</button>
                </div>
            </div>
            {/* Non-veg Meal Section */}
            <h2 className='text-success mt-5 mb-4'><span style={{color:'#D44848'}}>Non</span> -Veg</h2>
            <div className="row g-5">
                {nonvegMeal.map(meal => (
                    <div key={meal.id} className="col-md-4">
                        <div style={{boxShadow: 'rgba(0, 0, 0, 0.3) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px'}}>
                            <img style={{objectFit:'cover',width:'100%',height:'200px'}} src={`http://localhost:5000/uploads/${encodeURIComponent(meal.mealimage)}`} alt="img veg" />
                            <h6 className='text-center py-2 text-capitalize'>{meal.mealname}</h6>
                        </div>
                    </div>  
                ))}
            </div>
            {/* Upload Button */}
            <div className="row mb-4">
                <div className='w-100'>
                     <button className="btn d-block ms-auto btn-outline-primary me-5" onClick={() => handleButtonClick('non_veg')} data-bs-toggle="modal" data-bs-target=".uplodMeals">Upload+</button>
                </div>
            </div>
            <hr className='my-5' style={{border: '1px solid #000'}} />
            {/* Summer And Winter Meal Section  */}
            <h4 className='text-center mt-5'>Grab our exclusive weather-aware dining offer </h4>
            <h1 className='text-primary text-center'>Explore now!</h1>
            {/* Winter */}
            <h3 className='mt-5 mb-4'>Winter:</h3>
            <div className="row g-5">
                {winterMeal.map(meal => (
                    <div key={meal.id} className="col-md-4">
                        <div style={{boxShadow: 'rgba(0, 0, 0, 0.3) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px'}}>
                            <img style={{objectFit:'cover',width:'100%',height:'200px'}} src={`http://localhost:5000/uploads/${encodeURIComponent(meal.mealimage)}`} alt="img veg" />
                            <h6 className='text-center py-2 text-capitalize'>{meal.mealname}</h6>
                        </div>
                    </div>  
                ))}
            </div>
            {/* Upload Button */}
            <div className="row mb-4">
                <div className='w-100'>
                     <button className="btn d-block ms-auto btn-outline-primary me-5" onClick={() => handleButtonClick('winter')} data-bs-toggle="modal" data-bs-target=".uplodMeals">Upload+</button>
                </div>
            </div>
            {/* Summer*/}
            <h3 className='mt-5 mb-4'>Summer:</h3>
            <div className="row g-5 mb-5">
                {summerMeal.map(meal => (
                    <div key={meal.id} className="col-md-4">
                        <div style={{boxShadow: 'rgba(0, 0, 0, 0.3) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px'}}>
                            <img style={{objectFit:'cover',width:'100%',height:'200px'}} src={`http://localhost:5000/uploads/${encodeURIComponent(meal.mealimage)}`} alt="img veg" />
                            <h6 className='text-center py-2 text-capitalize'>{meal.mealname}</h6>
                        </div>
                    </div>  
                ))}
            </div>
            {/* Upload Button */}
            <div className="row mb-4">
                <div className='w-100'>
                     <button className="btn d-block ms-auto btn-outline-primary me-5" onClick={() => handleButtonClick('summer')} data-bs-toggle="modal" data-bs-target=".uplodMeals">Upload+</button>
                </div>
            </div>
        </section>

        {/* Upload Meal Modall */}
        <div className="modal fade uplodMeals" tabIndex="-1" aria-labelledby="todayMenuModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-lg modal-dialog-centered">
                <form className="modal-content" onSubmit={handleSubmit}>
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id="todayMenuModalLabel">Upload Meals {mealCategory.toUpperCase()}</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <div className="mb-3">
                            <label className="form-label">Meal Name</label>
                            <input required type="text" className="form-control" name="name" value={mealName} onChange={(e) => setMealName(e.target.value)} />
                        </div>
                        <div className="mb-3">
                            <label className="form-label">Meal Image</label>
                            <input required type="file" className="form-control" onChange={(e) => setMealImage(e.target.files[0])} />
                        </div>
                    </div>
                    {errorMessage && <div className="text-danger">{errorMessage}</div>}
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" style={{ background: '#e1680b' }} className="btn btn-primary text-white">Save Changes</button>
                    </div>
                </form>
            </div>
      </div>
      <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="colored"
                style={{zIndex:'9999999'}}
            />
    </AdminLayout>
  )
}

export default Home