import React, { useState, useEffect } from "react";
import axios from "axios";
import AdminLayout from "../../layouts/AdminLayout";

const AdminFeedback = () => {
  const [feedbacks, setFeedbacks] = useState([]);

  useEffect(() => {
    // Fetch feedbacks when the component mounts
    const fetchFeedbacks = async () => {
      try {
        const response = await axios.get("http://localhost:5000/api/getFeedbacks");
        setFeedbacks(response.data);
      } catch (error) {
        console.error("Error fetching feedbacks:", error);
      }
    };
    fetchFeedbacks();
  }, []); // Empty dependency array to run effect only once on mount

  return (
    <AdminLayout>
      <div className=" container-fluid px-md-5  px-3 mt-5">
        {feedbacks.map((feedback, index) => (
          <div key={index} className="bg-body-secondary p-3 rounded gap-2 mb-3">
            <h5>Name: {feedback.name}</h5>
            <h5>Subject: {feedback.subject}</h5>
            <h5>Message: {feedback.message}</h5>
          </div>
        ))}
      </div>
    </AdminLayout>
  );
};

export default AdminFeedback;



